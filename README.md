<!-- README.md -->


#####################################################
#####################################################

Author: Vikas Kumar
GitHub: @dzenvikas
Date: 03-Jan-2018




NOTE:

All svg and png files (icons, background illustrations) used in this project are designed and created by me (@dzenvikas) except playstore and appstore svg files and user images.


#####################################################
#####################################################





RUN THE PROJECT USING TERMINAL:


1. Navigate to the root of this project using terminal.

2. Install node modules:

		$ npm install

3. Run :
		
		$ gulp watch



#####################################################
#####################################################