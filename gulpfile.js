
/* Author: Vikas Kumar (@dzenvikas) */



var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browserSync', function()	{
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
});

gulp.task('watch', ['browserSync'], function(){
	gulp.watch('./*', browserSync.reload);
	gulp.watch('./vendors/css/*css', browserSync.reload);
	gulp.watch('./resources/css/*css',browserSync.reload);
});